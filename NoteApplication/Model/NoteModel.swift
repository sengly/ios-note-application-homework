//
//  NoteModel.swift
//  NoteApplication
//
//  Created by Sengly Sun on 12/12/19.
//  Copyright © 2019 Sengly Sun. All rights reserved.
//

import Foundation
struct NoteModel {
    var title: String
    var date: String
    var desc: String
}
