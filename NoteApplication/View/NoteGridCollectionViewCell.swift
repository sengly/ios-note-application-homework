//
//  NoteGridCollectionViewCell.swift
//  NoteApplication
//
//  Created by Sengly Sun on 12/12/19.
//  Copyright © 2019 Sengly Sun. All rights reserved.
//

import UIKit

class NoteGridCollectionViewCell: UICollectionViewCell {
    @IBOutlet var descriptionTextView: UITextView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var dateLabel: UILabel!
    var collectionViewCell = UICollectionViewCell()
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
