//
//  NoteService.swift
//  NoteApplication
//
//  Created by Sengly Sun on 12/12/19.
//  Copyright © 2019 Sengly Sun. All rights reserved.
//
import UIKit
import Foundation
import CoreData
class NoteService {
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    //Create New Note
    func createNewNote(noteModel: NoteModel){
        let entity = NSEntityDescription.entity(forEntityName: "Note", in: context)
        let newNote = Note(entity: entity!, insertInto: context)
        
        newNote.date = noteModel.date
        newNote.desc = noteModel.desc
        newNote.title = noteModel.title
        
        do {
            try context.save()
        } catch  {
            print("Failure Saving")
        }
        print("Saved")
        print(fetchNote().count)
    }
    
    //Fetch Note
    func fetchNote() -> [Note] {
        let allNotes: [Note]? = try? context.fetch(Note.fetchRequest())
        return allNotes!
    }
    
    //Update Note
    func updateNote(note: NoteModel, byTitle title: String){
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Note")
        fetchRequest.predicate = NSPredicate(format: "title = %@", "\(title)")
        
            let test = try? context.fetch(fetchRequest)
            
            let noteToUpdate = test![0] as! NSManagedObject
            noteToUpdate.setValue(note.title, forKey: "title")
            noteToUpdate.setValue(note.desc, forKey: "desc")

            do{
                try context.save()
            }catch {
                print(error)
            }
        
    }
    
    //Delete Note
    func deleteNote(byTitle title: String){
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Note")
        fetchRequest.predicate = NSPredicate(format: "title = %@", "\(title)")
        let test = try? context.fetch(fetchRequest)
        
        let noteToDelete = test![0] as! NSManagedObject
        context.delete(noteToDelete)

        do{
            try context.save()
        }catch {
            print(error)
        }
    }
}
