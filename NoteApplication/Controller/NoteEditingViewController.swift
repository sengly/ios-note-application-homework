//
//  NoteEditingViewController.swift
//  NoteApplication
//
//  Created by Sengly Sun on 12/12/19.
//  Copyright © 2019 Sengly Sun. All rights reserved.
//

import UIKit

class NoteEditingViewController: UIViewController {

    @IBOutlet var editNoteLabel: UILabel!
    @IBOutlet var titleLabel: UITextField!
    @IBOutlet var descriptionTextView: UITextView!
    var textTitle: String?
    var textDesc: String?
    var noteService: NoteService?
    override func viewDidLoad() {
        super.viewDidLoad()
        titleLabel.placeholder = "titleHere".localizedString()
        editNoteLabel.text = "editNote".localizedString()
        descriptionTextView.text = textDesc!
        titleLabel.text = textTitle!
        // Do any additional setup after loading the view.
    }
    
    
    //Update note
    override func viewWillDisappear(_ animated: Bool) {
        if self.isMovingFromParent{
            noteService = NoteService()
            let title = titleLabel.text
            let desc = descriptionTextView.text
            let date = Date()
            let format = DateFormatter()
            format.dateFormat = "yyyy-MM-dd"
            let formattedDate = format.string(from: date)
            let newNote = NoteModel(title: title!, date: formattedDate, desc: desc!)
            noteService?.updateNote(note: newNote, byTitle: textTitle!)
            navigationController?.popViewController(animated: true)
            print(newNote)
        }
    }
    
    //Editor Option action sheet
    @IBAction func editorOption(_ sender: Any) {
        let editorOption = UIAlertController()
        
        let takePhoto = UIAlertAction(title: "takePhoto".localizedString(), style: .default) { (_) in
        }
        
        let ChooseImage = UIAlertAction(title: "chooseImage".localizedString(), style: .default) { (_) in
            
        }
        
        let cancel = UIAlertAction(title: "cancel".localizedString(), style: .cancel) { (_) in
            
        }
        let drawing = UIAlertAction(title: "drawing".localizedString(), style: .default) { (_) in
        }
        
        let recording = UIAlertAction(title: "recording".localizedString(), style: .default) { (_) in
            
        }
        
        let checkBoxs = UIAlertAction(title: "checkboxs".localizedString(), style: .default) { (_) in
            
        }
        
        editorOption.addAction(takePhoto)
        editorOption.addAction(ChooseImage)
        editorOption.addAction(drawing)
        editorOption.addAction(recording)
        editorOption.addAction(checkBoxs)
        editorOption.addAction(cancel)
        
        present(editorOption, animated: true, completion: nil)
    }
    
    //Note option action sheet
    @IBAction func noteOption(_ sender: Any) {
        let noteOption = UIAlertController()
        
        let delete = UIAlertAction(title: "delete".localizedString(), style: .default) { (_) in
        }
        
        let makeACopy = UIAlertAction(title: "makeACopy".localizedString(), style: .default) { (_) in
            
        }
        
        let send = UIAlertAction(title: "send".localizedString(), style: .default) { (_) in
        }
        
        let collarbrators = UIAlertAction(title: "collarbrators".localizedString(), style: .default) { (_) in
            
        }
        
        let labels = UIAlertAction(title: "labels".localizedString(), style: .default) { (_) in
            
        }
        
        let cancel = UIAlertAction(title: "cancel".localizedString(), style: .cancel) { (_) in
            
        }
        noteOption.addAction(delete)
        noteOption.addAction(makeACopy)
        noteOption.addAction(send)
        noteOption.addAction(collarbrators)
        noteOption.addAction(labels)
        noteOption.addAction(cancel)
        
        present(noteOption, animated: true, completion: nil)
    }

}
