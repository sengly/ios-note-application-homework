//
//  ViewController.swift
//  NoteApplication
//
//  Created by Sengly Sun on 12/12/19.
//  Copyright © 2019 Sengly Sun. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource {
    @IBOutlet var notes: UILabel!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var collectionView: UICollectionView!
    
    var collectionViewFlowLayout: UICollectionViewFlowLayout!
    let cellIndentifier: String = "noteGridCell"
    var noteService: NoteService?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.dataSource = self
        collectionView.delegate = self
        setUpLongPressedGesture()
        setUpCollectionView()
        setUpTapGesture()
        
    }
    

    
    override func viewWillAppear(_ animated: Bool) {
        collectionView.reloadData()
        notes.text = "notes".localizedString()
        titleLabel.text = "Notes".localizedString()
    }
    
    //Tap Gesture
    func setUpTapGesture(){
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapGesture(_:)))
        tap.numberOfTapsRequired = 1
        tap.numberOfTouchesRequired = 1
        collectionView?.addGestureRecognizer(tap)
    }
    
    //Long pressed Gesture
    func setUpLongPressedGesture() {
        let longPressed = UILongPressGestureRecognizer(target: self, action: #selector(longPressedGesture(_:)))
        collectionView.addGestureRecognizer(longPressed)
    }
    
    //Function long pressed gesture to delete
    @objc func longPressedGesture(_ gesture: UILongPressGestureRecognizer){
           
        var notes:[Note]? = noteService?.fetchNote()
           let point = gesture.location(in: collectionView)
           
           if let indexPath = self.collectionView.indexPathForItem(at: point){
               
               let cell = self.collectionView.cellForItem(at: indexPath) as! NoteGridCollectionViewCell
               
            let alert = UIAlertController()
               
               alert.addAction(UIAlertAction(title: "Delete".localizedString(),
                                             style: .destructive,
                                             handler:
                { (action) in
                self.noteService?.deleteNote(byTitle: cell.titleLabel.text!)
                self.collectionView.reloadData()
                notes?.remove(at: indexPath.row)
               }))
               
               alert.addAction(UIAlertAction(title: "cancel".localizedString(), style: .cancel, handler: nil))
               
               self.present(alert, animated: true, completion: nil)
           }
       }
    //Function tap to edit note
    @objc func tapGesture(_ tapGesture: UITapGestureRecognizer) {
        let point = tapGesture.location(in: collectionView)
        if let indexPath = collectionView?.indexPathForItem(at: point) {
            
            let cell = self.collectionView.cellForItem(at: indexPath) as! NoteGridCollectionViewCell
            
            let title = cell.titleLabel.text!
            let desc = cell.descriptionTextView.text!
            
            print("\(title) \(desc)")
            
            let storyboard = UIStoryboard(name: "EditNoteStoryboard", bundle: Bundle.main)
            let editVC = storyboard.instantiateViewController(identifier: "NoteEditingViewController") as NoteEditingViewController

            editVC.textTitle = title
            editVC.textDesc = desc
            
            navigationController?.pushViewController(editVC, animated: true)
        
        }
    }
   
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        setUpCollectViewCellItemSize()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        noteService = NoteService()
        return noteService!.fetchNote().count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIndentifier, for: indexPath) as! NoteGridCollectionViewCell
        let notes:[Note]? = noteService?.fetchNote()
        cell.dateLabel?.text = notes![indexPath.row].date
        cell.descriptionTextView?.text = notes![indexPath.row].desc
        cell.titleLabel?.text = notes![indexPath.row].title
        return cell
    }
    
    private func setUpCollectionView() {
        collectionView.register(UINib(nibName: "NoteGridCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: cellIndentifier)
    }
    
    
    //set up item size in collection view
    private func setUpCollectViewCellItemSize(){
        if collectionViewFlowLayout == nil {
            let itemPerRow: CGFloat =  2
            let lineSpacing: CGFloat = 10
            let itemSpacing: CGFloat = 10
            let width = (collectionView.frame.width - (itemPerRow - 1) * (itemSpacing)) / itemPerRow
            let height = width
            
            collectionViewFlowLayout = UICollectionViewFlowLayout()
            collectionViewFlowLayout.itemSize = CGSize(width: width, height: height)
            collectionViewFlowLayout.sectionInset = UIEdgeInsets.zero
            collectionViewFlowLayout.scrollDirection = .vertical
            collectionViewFlowLayout.minimumLineSpacing = lineSpacing
            collectionViewFlowLayout.minimumInteritemSpacing = itemSpacing
            
            collectionView.setCollectionViewLayout(collectionViewFlowLayout, animated: true)
        }
    }
    
    //Localization button bar
    @IBAction func switchLanguage(_ sender: Any) {
        let buttonBarSwitchLanguage = UIAlertController()
        
        let khmer = UIAlertAction(title: "khmer".localizedString(), style: .default) { (_) in
            LanguageApp.shared.chooseLanguage(language: .khmer)
        }
        
        let english = UIAlertAction(title: "english".localizedString(), style: .default) { (_) in
            LanguageApp.shared.chooseLanguage(language: .english)
        }
        
        let cancel = UIAlertAction(title: "cancel".localizedString(), style: .cancel) { (_) in
            
        }
        
        buttonBarSwitchLanguage.addAction(khmer)
        buttonBarSwitchLanguage.addAction(english)
        buttonBarSwitchLanguage.addAction(cancel)
        
        present(buttonBarSwitchLanguage, animated: true, completion: nil)
        
    }
    
    //Switch note view mode button
    @IBAction func switchViewMode(_ sender: Any) {
        let buttonSwitchViewMode = UIAlertController()
        let grid = UIAlertAction(title: "viewAsGrid".localizedString(), style: .default) { (_) in
        }
        
        let row = UIAlertAction(title: "viewAsRow".localizedString(), style: .default) { (_) in
            
        }
        
        let cancel = UIAlertAction(title: "cancel".localizedString(), style: .cancel) { (_) in
            
        }
        
        buttonSwitchViewMode.addAction(grid)
        buttonSwitchViewMode.addAction(row)
        buttonSwitchViewMode.addAction(cancel)
        
        present(buttonSwitchViewMode, animated: true, completion: nil)
    }
}
extension String{
    func localizedString() -> String {
        let path = Bundle.main.path(forResource: LanguageApp.shared.language, ofType: "lproj")
        let bundle = Bundle(path: path!)
        return NSLocalizedString(self, tableName: nil, bundle: bundle! , value: self, comment: self)
    }
}
